use crate::http::request::ParseError;
use crate::http::request::Request;
use crate::http::Response;
use std::convert::TryFrom;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Read;
use std::net::TcpListener;

pub trait Handler {
    fn handle_request(&mut self, request: &Request) -> Response;
    fn handle_bad_request(&mut self, error: &ParseError) -> Response {
        println!("Failed to parse request: {}", error);
        Response::new(crate::http::StatusCode::BadRequest, None)
    }
}

pub struct Server {
    ip: String,
    port: String,
}

impl Server {
    pub fn new(address: &str) -> Result<Self, Error> {
        let parts: Vec<&str> = address.split(":").collect();
        if parts.len() == 2 {
            let server = Self {
                ip: String::from(parts[0]),
                port: String::from(parts[1]),
            };
            Ok(server)
        } else {
            Err(Error::new(ErrorKind::Other, "invalid value"))
        }
    }

    pub fn run(&self, mut handler: impl Handler) {
        println!("Running at {}:{}", &self.ip, &self.port);
        let listener = TcpListener::bind(format!("{}:{}", &self.ip, &self.port)).unwrap();

        loop {
            match listener.accept() {
                Ok((mut stream, _addr)) => {
                    let mut buffer = [0; 1024];
                    match stream.read(&mut buffer) {
                        Ok(_) => {
                            let response = match Request::try_from(&buffer as &[u8]) {
                                Ok(request) => {
                                    dbg!(&request);
                                    handler.handle_request(&request)
                                }
                                Err(e) => {
                                    println!("Failed to parse response: {}", e);
                                    handler.handle_bad_request(&e)
                                }
                            };
                            response.send(&mut stream).unwrap();
                        }
                        Err(e) => {
                            println!("Failed to read from connection: {}", e);
                        }
                    }
                }
                Err(e) => {
                    println!("Failed to establish a connection: {}", e);
                }
            }
        }
    }
}
